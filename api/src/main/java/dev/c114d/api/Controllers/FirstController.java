package dev.c114d.api.Controllers;


import dev.c114d.api.Handlers.DatabaseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/first")
public class FirstController {
    @Autowired
    private DatabaseHandler databaseHandler;

    @GetMapping("/classes")
    public List<Object> getAllClasses() { return databaseHandler.getAllClasses(); }
    @PostMapping(value = "/assignClass", params = {"usernameTeacher", "subjectName", "classTypeName"})
    public void assignClass(@RequestParam(value = "usernameTeacher") String usernameTeacher,
                            @RequestParam(value = "subjectName") String subjectName,
                            @RequestParam(value = "classTypeName") String classTypeName) {
        databaseHandler.assignClass(usernameTeacher, subjectName, classTypeName);
    }

    @GetMapping("/classtypes")
    public List<Object> getAllClassTypes() { return databaseHandler.getAllClassTypes(); }

    @GetMapping("/grouplists")
    public List<Object> getAllGroupLists() { return databaseHandler.getAllGroupLists(); }
    @PostMapping(value = "/assignGroupToClass", params = {"usernameTeacher", "subjectName", "classTypeName",
                                                            "groupName"})
    public void assignGroupToClass(@RequestParam(value = "usernameTeacher") String usernameTeacher,
                               @RequestParam(value = "subjectName") String subjectName,
                               @RequestParam(value = "classTypeName") String classTypeName,
                               @RequestParam(value = "groupName") String groupName){
        databaseHandler.assignGroupToClass(usernameTeacher, subjectName, classTypeName,
                groupName);
    }

    @GetMapping("/groups")
    public List<Object> getAllGroups() { return databaseHandler.getAllGroups(); }
    @PostMapping(value = "/addGroup", params = {"groupName"})
    public void addGroup(@RequestParam(value = "groupName") String groupName) {
        databaseHandler.addGroup(groupName);
    }

    @GetMapping("/roles")
    public List<Object> getAllRoles()
    {
        return databaseHandler.getAllRoles();
    }

    @GetMapping("/specializations")
    public List<Object> getAllSpecializations() { return databaseHandler.getAllSpecializations(); }
    @PostMapping(value = "/addSpecialization", params = {"specialization"})
    public void addSpecialization(@RequestParam(value = "specialization") String specialization) {
        databaseHandler.addSpecialization(specialization);
    }

    @GetMapping("/students")
    public List<Object> getAllStudents() { return databaseHandler.getAllStudents(); }

    @GetMapping("/studentuploads")
    public List<Object> getAllStudentUploads() { return databaseHandler.getAllStudentUploads(); }

    @GetMapping("/subjects")
    public List<Object> getAllSubjects() { return databaseHandler.getAllSubjects(); }
    @PostMapping(value = "/addSubject", params = {"subjectName", "classTypeName"})
    public void addSubject(@RequestParam(value = "subjectName") String subjectName,
                           @RequestParam(value = "classTypeName") String classTypeName) {
        databaseHandler.addSubject(subjectName, classTypeName);
    }

    @GetMapping("/teacherpost")
    public List<Object> getAllTeacherPosts() { return databaseHandler.getAllTeacherPosts(); }
    @PostMapping(value = "/addTeacherPost", params = {"usernameTeacher", "subjectName", "classTypeName",
                                                      "startTime", "endTime", "postName"})
    public void addTeacherPost(@RequestParam(value = "usernameTeacher") String usernameTeacher,
                               @RequestParam(value = "subjectName") String subjectName,
                               @RequestParam(value = "classTypeName") String classTypeName,
                               @RequestParam(value = "startTime") String startTime,
                               @RequestParam(value = "endTime") String endTime,
                               @RequestParam(value = "postName") String postName){
        databaseHandler.addTeacherPost(usernameTeacher, subjectName, classTypeName,
                startTime, endTime, postName);
    }

    @GetMapping("/users")
    public List<Object> getAllUsers()
    {
        return databaseHandler.getAllUsers();
    }
    @PostMapping(value = "/addAdmin", params = {"surname","firstname","mailAddress","username"})
    public void addAdmin(@RequestParam(value = "surname") String surname,
                         @RequestParam(value = "firstname") String firstname,
                         @RequestParam(value = "mailAddress") String mailAddress,
                         @RequestParam(value = "username") String username) {
        databaseHandler.addAdmin(surname, firstname, mailAddress, username);
    }
    @PostMapping(value = "/addProfesor", params = {"surname","firstname","mailAddress","username"})
    public void addProfesor(@RequestParam(value = "surname") String surname,
                            @RequestParam(value = "firstname") String firstname,
                            @RequestParam(value = "mailAddress") String mailAddress,
                            @RequestParam(value = "username") String username) {
        databaseHandler.addProfesor(surname, firstname, mailAddress, username);
    }
    @PostMapping(value = "/addStudent", params = {"surname","firstname","mailAddress","username",
                                                  "groupName","specializationName"})
    public void addStudent(@RequestParam(value = "surname") String surname,
                           @RequestParam(value = "firstname") String firstname,
                           @RequestParam(value = "mailAddress") String mailAddress,
                           @RequestParam(value = "username") String username,
                           @RequestParam(value = "groupName") String groupName,
                           @RequestParam(value = "specializationName") String specializationName) {
        databaseHandler.addStudent(surname, firstname, mailAddress, username, groupName, specializationName);
    }

}
