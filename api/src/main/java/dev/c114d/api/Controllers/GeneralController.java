package dev.c114d.api.Controllers;

import dev.c114d.api.Handlers.DatabaseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/general")
public class GeneralController {

    @Autowired
    private DatabaseHandler databaseHandler;

    @GetMapping(value = "/getUserId", params = {"username"})
    ResponseEntity getUserId(@RequestParam(value = "username") String username){
        return  new ResponseEntity<>(databaseHandler.getUserId(username), HttpStatus.OK);
    }

    @GetMapping(value = "/getResultsForTeacherPost", params = {"idTeacherPost"})
    ResponseEntity getResultsForTeacherPost(@RequestParam(value = "idTeacherPost") Integer idTeacherPost){
        return  new ResponseEntity<>(databaseHandler.getResultsForTeacherPost(idTeacherPost), HttpStatus.OK);
    }
}
