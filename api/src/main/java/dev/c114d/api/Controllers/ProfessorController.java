package dev.c114d.api.Controllers;

import dev.c114d.api.Handlers.DatabaseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/professor")
public class ProfessorController {
    @Autowired
    private DatabaseHandler databaseHandler;

    @GetMapping(value = "/classesTaught", params = {"idProfessor"})
    ResponseEntity getClassesTaught(@RequestParam(value = "idProfessor") Integer idProfessor){
        return  new ResponseEntity<>(databaseHandler.getClassesTaught(idProfessor), HttpStatus.OK);
    }

    @GetMapping(value = "/assignmentsGivenForSubject", params = {"idProfessor", "idSubject"})
    ResponseEntity getAssignmentsGivenForSubject(@RequestParam(value = "idProfessor") Integer idProfessor,
                                                 @RequestParam(value = "idSubject") Integer idSubject){
        return  new ResponseEntity<>(databaseHandler.getAssignmentsGivenForSubject(idProfessor, idSubject), HttpStatus.OK);
    }
}
