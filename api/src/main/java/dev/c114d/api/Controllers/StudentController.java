package dev.c114d.api.Controllers;

import dev.c114d.api.Handlers.DatabaseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private DatabaseHandler databaseHandler;

    @GetMapping(value = "/classesattended", params = {"idStudent"})
    ResponseEntity getClassesAttended(@RequestParam(value = "idStudent") Integer idStudent){
        return  new ResponseEntity<>(databaseHandler.getClassesAttended(idStudent), HttpStatus.OK);
    }

    @GetMapping(value = "/assignmentsforsubject", params = {"idStudent", "idSubject"})
    ResponseEntity getAssignmentsForSubject(@RequestParam(value = "idStudent") Integer idStudent,
                                            @RequestParam(value = "idSubject") Integer idSubject){
        return  new ResponseEntity<>(databaseHandler.getAssignmentsForSubject(idStudent, idSubject), HttpStatus.OK);
    }

}
