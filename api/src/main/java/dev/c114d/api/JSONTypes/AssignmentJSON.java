package dev.c114d.api.JSONTypes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AssignmentJSON {
    private OffsetDateTime startTime;
    private OffsetDateTime stopTime;
    private Integer idTeacherPost;
    private String postName;
    private String assignmentFilePath;
    private String assignmentContent;
}
