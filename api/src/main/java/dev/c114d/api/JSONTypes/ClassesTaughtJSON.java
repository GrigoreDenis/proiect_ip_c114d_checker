package dev.c114d.api.JSONTypes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClassesTaughtJSON {
    private Integer idSubject;
    private String subjectName;
    private String classTypeName;
}
