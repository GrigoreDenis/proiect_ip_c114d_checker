package dev.c114d.api.JSONTypes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResultJSON {
    private Integer idStudentUpload;
    private String postName;
    private String username;
    private String firstname;
    private String surname;
    private String groupName;
    private String resultsFilePath;
    private String result;
    private OffsetDateTime timestamp;
}
