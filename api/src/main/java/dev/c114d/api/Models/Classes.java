package dev.c114d.api.Models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class Classes {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer idClass;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idSubject", nullable = false)
    private Subjects subject;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idUserTeacher", nullable = false)
    private Users userTeacher;

    @OneToMany(mappedBy = "classs")
    private Set<GroupLists> classsGroupListss;

    @OneToMany(mappedBy = "classs")
    private Set<TeacherPost> classsTeacherPosts;

}

