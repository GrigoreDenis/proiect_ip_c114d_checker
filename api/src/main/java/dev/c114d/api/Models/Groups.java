package dev.c114d.api.Models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class Groups {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer idGroup;

    @Column(nullable = false, length = 50)
    private String groupName;

    @OneToMany(mappedBy = "group")
    private Set<GroupLists> groupGroupListss;

    @OneToMany(mappedBy = "group")
    private Set<Students> groupStudentss;

}

