package dev.c114d.api.Models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class Specializations {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer idSpecialization;

    @Column(nullable = false, length = 50, name = "specializationName")
    private String specializationName;

    @OneToMany(mappedBy = "specialization")
    private Set<Students> specializationStudentss;

}

