package dev.c114d.api.Models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.time.OffsetDateTime;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class StudentUpload {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer idStudentUpload;

    @Column(nullable = false)
    private OffsetDateTime timestamp;

    @Column(nullable = false, length = 70)
    private String resultsFilePath;

    @Column(nullable = false, length = 70)
    private String sourceCodeFilePath;

    @Column(nullable = false, length = 3)
    private String result;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idStudent", nullable = false)
    private Students student;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idTeacherPost", nullable = false)
    private TeacherPost teacherPost;

}
