package dev.c114d.api.Models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class Subjects {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer idSubject;

    @Column(nullable = false, length = 50)
    private String subjectName;

    @OneToMany(mappedBy = "subject")
    private Set<Classes> subjectClassess;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idClassType", nullable = false)
    private ClassTypes classType;

}

