package dev.c114d.api.Models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import java.time.OffsetDateTime;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class TeacherPost {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer idTeacherPost;

    @Column(nullable = false, length = 50)
    private String postName;

    @Column(nullable = false)
    private OffsetDateTime timestamp;

    @Column(nullable = false)
    private OffsetDateTime startTime;

    @Column(nullable = false)
    private OffsetDateTime stopTime;

    @Column(nullable = true, length = 70)
    private String assignmentFilePath;

    @Column(nullable = true, length = 3500)
    private String assignmentContent;

    @OneToMany(mappedBy = "teacherPost")
    private Set<StudentUpload> teacherPostStudentUploads;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idClass", nullable = false)
    private Classes classs;

}

