package dev.c114d.api.Repository;

import dev.c114d.api.Models.ClassTypes;
import dev.c114d.api.Models.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassTypesRepository extends JpaRepository<ClassTypes, Integer> {

    @Query(value = "select * from ClassTypes", nativeQuery = true)
    List<Object> getClassTypes();

    ClassTypes findByClassTypeName(String classTypeName);
}