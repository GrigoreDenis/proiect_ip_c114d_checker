package dev.c114d.api.Repository;

import dev.c114d.api.Models.ClassTypes;
import dev.c114d.api.Models.Classes;
import dev.c114d.api.Models.Subjects;
import dev.c114d.api.Models.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassesRepository extends JpaRepository<Classes, Integer> {

    @Query(value = "select * from Classes", nativeQuery = true)
    List<Object> getClasses();

    Classes findBySubjectAndUserTeacher(Subjects subject, Users userTeacher);
}
