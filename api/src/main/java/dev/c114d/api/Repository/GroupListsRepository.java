package dev.c114d.api.Repository;

import dev.c114d.api.Models.GroupLists;
import dev.c114d.api.Models.Groups;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupListsRepository extends JpaRepository<GroupLists, Integer> {

    @Query(value = "select * from GroupLists", nativeQuery = true)
    List<Object> getGroupLists();

    List<GroupLists> findGroupListByGroup(Groups group);
}