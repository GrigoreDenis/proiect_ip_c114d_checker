package dev.c114d.api.Repository;

import dev.c114d.api.Models.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RolesRepository extends JpaRepository<Roles, Integer> {

    @Query(value = "select * from Roles", nativeQuery = true)
    List<Object> getRoles();

    Roles findByDescription(String description);

}
