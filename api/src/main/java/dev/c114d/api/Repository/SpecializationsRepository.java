package dev.c114d.api.Repository;

import dev.c114d.api.Models.Groups;
import dev.c114d.api.Models.Specializations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpecializationsRepository extends JpaRepository<Specializations, Integer> {

    @Query(value = "select * from Specializations", nativeQuery = true)
    List<Object> getSpecializations();

    Specializations findBySpecializationName(String specializationName);
}