package dev.c114d.api.Repository;

import dev.c114d.api.Models.StudentUpload;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentUploadRepository extends JpaRepository<StudentUpload, Integer> {

    @Query(value = "select * from StudentUpload", nativeQuery = true)
    List<Object> getStudentUploads();

}