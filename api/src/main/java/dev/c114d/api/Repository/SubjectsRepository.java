package dev.c114d.api.Repository;

import dev.c114d.api.Models.ClassTypes;
import dev.c114d.api.Models.Subjects;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectsRepository extends JpaRepository<Subjects, Integer> {

    @Query(value = "select * from Subjects", nativeQuery = true)
    List<Object> getSubjects();

    Subjects findBySubjectNameAndClassType(String subjectName, ClassTypes classType);
}