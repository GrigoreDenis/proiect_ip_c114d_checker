package dev.c114d.api.Repository;

import dev.c114d.api.Models.Classes;
import dev.c114d.api.Models.TeacherPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherPostRepository extends JpaRepository<TeacherPost, Integer> {

    @Query(value = "select * from TeacherPost", nativeQuery = true)
    List<Object> getTeacherPosts();

    List<TeacherPost> findByClasss(Classes givenClass);
}
