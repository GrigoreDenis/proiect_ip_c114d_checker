package dev.c114d.api.Repository;

import dev.c114d.api.Models.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {

    @Query(value = "select * from Users", nativeQuery = true)
    List<Object> getUsers();

    Users findByUsername(String username);

    Users findByIdUser(Integer idUser);
}